package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class MainTest {

  private final  int  COUNT = 5;
  public static String[][] testScenario = {
          {
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msisdn\": \"88005553535\"" +
                          "} ",
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msisdn\": \"88005553535\"," +
                          "    \"enrichment\": {"+
                          "                     \"firstName\": \"Vasya\"," +
                          "                     \"lastName\": \"Ivanov\"" +
                          "                     }" +
                          "}"},
          {
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msisdn\": \"88005553535\"," +
                          "    \"enrichment\": {"+
                          "                     \"firstName\": \"test\"," +
                          "                     \"lastName\": \"test\"" +
                          "                     }" +
                          "} ",
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msisdn\": \"88005553535\"," +
                          "    \"enrichment\": {"+
                          "                     \"firstName\": \"Vasya\"," +
                          "                     \"lastName\": \"Ivanov\"" +
                          "                     }" +
                          "}"},
//
          {
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msis111dn\": \"88005553535\"" +
                          "} ",
                  "{" +
                          "    \"action\": \"button_click\"," +
                          "    \"page\": \"book_card\"," +
                          "    \"msis111dn\": \"88005553535\"" +
                          "} "},
          {"12345","12345"}

            };

  @Test
  void sanityTest() {
    assertTrue(true);
    String s;
    String s2;
    for (int i = 0; i < testScenario.length; i++ ) {
      s = new EnrichmentService().enrich(new Message(testScenario[i][0])).replaceAll("\\s","");
      s2 = testScenario[i][1].replaceAll("\\s","");
      assertTrue( s.equalsIgnoreCase( s2));
    }
  }


  @Test
  void multiTheadTest(){

    ExecutorService executor;
    executor = Executors.newFixedThreadPool(3);

    CountDownLatch latcher1 = new CountDownLatch(COUNT);
    CountDownLatch latcher2 = new CountDownLatch(COUNT);
    CountDownLatch latcher3 = new CountDownLatch(COUNT);
    CountDownLatch latcher4 = new CountDownLatch(COUNT);

    Future f1 = executor.submit(new TestThread(latcher1));
    Future f2 = executor.submit(new TestThread(latcher2));
    Future f3 = executor.submit(new TestThread(latcher3));
    Future f4 = executor.submit(new TestThread(latcher4));

    try {
      latcher1.await();
      latcher2.await();
      latcher3.await();
      latcher4.await();
    } catch(Exception e) { }

    executor.shutdown();

    assertTrue(true);
  }


  private class TestThread implements Runnable {

    CountDownLatch latch;
    public TestThread(CountDownLatch _latcher) {
      latch = _latcher;
    }

    @Override
    public void run() {
      try {
        for(int i = 0; i < COUNT; i++) {
          sanityTest();
          latch.countDown();
          Thread.sleep((long)(Math.random()*1000));
        }

      } catch (Exception e) {}
    }
  }


}