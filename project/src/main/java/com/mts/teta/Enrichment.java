package com.mts.teta;

public class Enrichment {
    public String firstName;
    public String lastName;

    public Enrichment(String _firstName, String _lastName) {
        firstName = _firstName;
        lastName = _lastName;
    }
}
