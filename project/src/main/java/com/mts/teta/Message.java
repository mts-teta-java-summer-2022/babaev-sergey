package com.mts.teta;


import com.google.gson.Gson;

/**
 * Message класс-родитель сообщений
 */
public class Message {
    private String content;
    public EnrichmentType enrichmentType;

    public Request request;


    /**
     * parse - парсер json. использует библиотеку com.google.gson
     * разбирает входящее сообщение в структурированный вид
     */
    public void parse() {
        Gson gson = new Gson();
        try {
            request = gson.fromJson(content, Request.class);
            if (request != null) {
                if (request.msisdn != null) {
                    enrichmentType = EnrichmentType.MSISDN;
                }
            }
        }
        catch (Exception e) {
            request = null;
        }

    }

    public enum EnrichmentType {
        MSISDN;
    }

    public String getAnswer(){
        Gson gson = new Gson();
        return enrichmentType == EnrichmentType.MSISDN ? gson.toJson(request) : content ;
    }

    public Message(String _content){
        //gson
        content = _content;

    }

}
