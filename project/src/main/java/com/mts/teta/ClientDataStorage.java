package com.mts.teta;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/** ClientDataStorage "мусорный" класс-прослойка для доступа к данным
 * класс статический, предназначен для чтения данных.
 * не создавать экземпляры класса
 */
public class ClientDataStorage extends CopyOnWriteArrayList<ClientData> {
    private static ClientDataStorage storage = new ClientDataStorage();

    public static ClientDataStorage getReference()
    {
        return storage;
    }

    private ClientDataStorage()
    {
        super();
        defaultInitialize();
    }

    /**
     * defaultInitialize метод генерации первичных данных
     */
    private void defaultInitialize() {
        addClient("88005553535", "Vasya", "Ivanov");
    }

    private ClientData addClient(String _msisdn, String _firstName, String _lastName ) {
        ClientData client = this.get(_msisdn);
        if (client != null){
            client.updateName(_firstName, _lastName);
        }
        else {
            this.add(new ClientData(_msisdn,  _firstName,  _lastName));
        }

        return client;
    }

    public ClientData get(String _msisdn) {
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).msisdn.equalsIgnoreCase(_msisdn))
                return this.get(i);
        }
        return null;
    }
}
