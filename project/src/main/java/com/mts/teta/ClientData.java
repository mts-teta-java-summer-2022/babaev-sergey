package com.mts.teta;

public class ClientData {
    public String msisdn;
    public String firstName;
    public String lastName;
    public ClientData(String _msisdn, String _firstName, String _lastName)
    {
        super();
        msisdn = _msisdn;
        firstName = _firstName;
        lastName = _lastName;
    }

    public void updateName(String _firstName, String _lastName) {
        firstName = _firstName;
        lastName = _lastName;
    }
}
