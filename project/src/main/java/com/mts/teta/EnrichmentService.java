package com.mts.teta;

import java.util.concurrent.ArrayBlockingQueue;

public class EnrichmentService {
    // возвращается обогащенный (или необогащенный content сообщения)

    protected static ClientDataStorage dataBase = ClientDataStorage.getReference();

    private ArrayBlockingQueue<Message> positiveQueue;
    private ArrayBlockingQueue<Message> negativeQueue;

    public EnrichmentService(){
        positiveQueue = new ArrayBlockingQueue(100);
        negativeQueue = new ArrayBlockingQueue(100);
    }
    public synchronized String enrich(Message message) {
        message.parse();

        if (message.enrichmentType == Message.EnrichmentType.MSISDN){
            if (enrichMSISDN(message.request)){
                positiveQueue.add(message);
            }
            else {
                negativeQueue.add(message);
            }
        }
        else {
            negativeQueue.add(message);
        }

        return message.getAnswer();
    }

    private boolean enrichMSISDN(Request request) {
        boolean retVal = false;
        ClientData client = dataBase.get(request.msisdn);
        if (client != null){
            request.enrichment = new Enrichment(client.firstName, client.lastName);
            retVal = true;
        }
        return retVal;

    }
}
